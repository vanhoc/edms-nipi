﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Globalization;

namespace TestFolderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Aspose.Cells;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The program.
    /// </summary>
    class Program
    {
        /// <summary>
        /// The is create.
        /// </summary>
        private bool isCreate = false;

        /// <summary>
        /// The file icon.
        /// </summary>
        private static Dictionary<string, string> fileIcon = new Dictionary<string, string>()
                {
                    { "doc", "images/wordfile.png" },
                    { "docx", "images/wordfile.png" },
                    { "dotx", "images/wordfile.png" },
                    { "xls", "images/excelfile.png" },
                    { "xlsx", "images/excelfile.png" },
                    { "pdf", "images/pdffile.png" },
                    { "7z", "images/7z.png" },
                    { "dwg", "images/dwg.png" },
                    { "dxf", "images/dxf.png" },
                    { "rar", "images/rar.png" },
                    { "zip", "images/zip.png" },
                    { "txt", "images/txt.png" },
                    { "xml", "images/xml.png" },
                    { "xlsm", "images/excelfile.png" },
                    { "bmp", "images/bmp.png" },
                };

        /// <summary>
        /// The folder service.
        /// </summary>
        private static FolderService folderService = new FolderService();

        /// <summary>
        /// The document service.
        /// </summary>
        private static DocumentService documentService = new DocumentService();

        private static RevisionService revisionService = new RevisionService();
        private static StatusService statusService = new StatusService();
        private static DisciplineService disciplineService = new DisciplineService();
        private static DocumentTypeService documentTypeService = new DocumentTypeService();
        private static ReceivedFromService receivedFromService = new ReceivedFromService();

        private static PermissionWorkgroupService PermissionWorkgroupService = new PermissionWorkgroupService();

        private static AttachFileService attachFileService = new AttachFileService();

        /// <summary>
        /// The file extensions.
        /// </summary>
        private static List<string> fileExtensions = ConfigurationSettings.AppSettings.Get("Extension").Split(',').ToList();

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        static void Main(string[] args)
        {
            #region Old code
            ////var tempUri = new Uri(@"\\WIN-P7KS57HL1HG\DocumentLibrary\Admin & HR11");

            ////const string MonitorPath = @"C:\Workspace\BD-EDMSmini\Dev\Web\EDMs.Web\DocumentLibrary";
            ////const string MonitorPath1 = @"C:\Workspace\BD-EDMSmini\Dev\Web\EDMs1Web\DocumentLibrary\Operation";

            ////var temp = MonitorPath1.Substring(0, MonitorPath.IndexOf(@"\DocumentLibrary"));

            ////MyFileSystemWatcher folderWatcher = new MyFileSystemWatcher(MonitorPath, "*.*");

            ////folderWatcher.Created += new System.IO.FileSystemEventHandler(fsw_Created);
            ////folderWatcher.Changed += new System.IO.FileSystemEventHandler(fsw_Changed);
            ////folderWatcher.Deleted += new System.IO.FileSystemEventHandler(fsw_Deleted);
            ////folderWatcher.Renamed += new System.IO.RenamedEventHandler(fsw_Renamed);
            ////folderWatcher.EnableRaisingEvents = true;

            ////////Console.WriteLine("Folder watcher is started on '{0}'", MonitorPath);

            ////var tempDoc = new Document();

            // Create folder
            ////Console.WriteLine(ConfigurationSettings.AppSettings.Get("rootPath"));
            ////Console.WriteLine(ConfigurationSettings.AppSettings.Get("listFolder"));
            ////CreateFolder();

            /*var categoryId = 6;
            var folderId = 319;
            var listDocuments = documentService.GetAllByFolder(folderId);

            var revisionList = revisionService.GetAll();
            var statusList = statusService.GetAllByCategory(categoryId);
            var disciplineList = disciplineService.GetAllByCategory(categoryId);
            var documentTypeList = documentTypeService.GetAllByCategory(categoryId);
            var receivedFromList = receivedFromService.GetAllByCategory(categoryId);
            
            var workbook = new Workbook();
            // Get the first worksheet.
            var worksheet1 = workbook.Worksheets[0];

            // Add a new worksheet and access it.
            var i = workbook.Worksheets.Add();

            var worksheet2 = workbook.Worksheets[i];

            // Create a range in the second worksheet.
            var rangeRevisionList = worksheet2.Cells.CreateRange("A1", "A" + revisionList.Count);
            var rangeStatusList = worksheet2.Cells.CreateRange("B1", "B" + statusList.Count);
            var rangeDisciplineList = worksheet2.Cells.CreateRange("C1", "C" + disciplineList.Count);
            var rangeDocumentTypeList = worksheet2.Cells.CreateRange("D1", "D" + documentTypeList.Count);
            var rangeReceivedFromList = worksheet2.Cells.CreateRange("E1", "E" + receivedFromList.Count);

            // Name the range.
            rangeRevisionList.Name = "RevisionList";
            rangeStatusList.Name = "StatusList";
            rangeDisciplineList.Name = "DisciplineList";
            rangeDocumentTypeList.Name = "DocumentTypeList";
            rangeReceivedFromList.Name = "ReceivedFromList";

            // Fill different cells with data in the range.
            for (int j = 0; j < revisionList.Count; j++)
            {
                rangeRevisionList[j, 0].PutValue(revisionList[j].Name);
            }

            for (int j = 0; j < statusList.Count; j++)
            {
                rangeStatusList[j, 0].PutValue(statusList[j].Name);
            }

            for (int j = 0; j < disciplineList.Count; j++)
            {
                rangeDisciplineList[j, 0].PutValue(disciplineList[j].Name);
            }

            for (int j = 0; j < documentTypeList.Count; j++)
            {
                rangeDocumentTypeList[j, 0].PutValue(documentTypeList[j].Name);
            }

            for (int j = 0; j < receivedFromList.Count; j++)
            {
                rangeReceivedFromList[j, 0].PutValue(receivedFromList[j].Name);
            }

            // Get the validations collection.
            var validations = worksheet1.Validations;
            CreateValidation("RevisionList", validations, 1, listDocuments.Count, 4, 4);
            CreateValidation("StatusList", validations, 1, listDocuments.Count, 5, 5);
            CreateValidation("DisciplineList", validations, 1, listDocuments.Count, 6, 6);
            CreateValidation("DocumentTypeList", validations, 1, listDocuments.Count, 7, 7);
            CreateValidation("ReceivedFromList", validations, 1, listDocuments.Count, 8, 8);

            worksheet1.Cells[0, 0].PutValue("ID");
            worksheet1.Cells[0, 1].PutValue("Name");
            worksheet1.Cells[0, 2].PutValue("Document number");
            worksheet1.Cells[0, 3].PutValue("Title");
            worksheet1.Cells[0, 4].PutValue("Revision");
            worksheet1.Cells[0, 5].PutValue("Status");
            worksheet1.Cells[0, 6].PutValue("Discipline");
            worksheet1.Cells[0, 7].PutValue("Document Type");
            worksheet1.Cells[0, 8].PutValue("Received from");
            worksheet1.Cells[0, 9].PutValue("Transmittal number");
            worksheet1.Cells[0, 10].PutValue("Received date");
            worksheet1.Cells[0, 11].PutValue("Remark");
            worksheet1.Cells[0, 12].PutValue("Well");
            for (int j = 0; j < listDocuments.Count; j++)
            {
                worksheet1.Cells[j + 1, 0].PutValue(listDocuments[j].ID);
                worksheet1.Cells[j + 1, 1].PutValue(listDocuments[j].Name);
                worksheet1.Cells[j + 1, 2].PutValue(listDocuments[j].DocumentNumber);
                worksheet1.Cells[j + 1, 3].PutValue(listDocuments[j].Title);
                worksheet1.Cells[j + 1, 4].PutValue(listDocuments[j].RevisionName);
                worksheet1.Cells[j + 1, 5].PutValue(listDocuments[j].Status == null ? string.Empty : listDocuments[j].Status.Name);
                worksheet1.Cells[j + 1, 6].PutValue(listDocuments[j].Discipline == null ? string.Empty : listDocuments[j].Discipline.Name);
                worksheet1.Cells[j + 1, 7].PutValue(listDocuments[j].DocumentType == null ? string.Empty : listDocuments[j].DocumentType.Name);
                worksheet1.Cells[j + 1, 8].PutValue(listDocuments[j].ReceivedFrom == null ? string.Empty : listDocuments[j].ReceivedFrom.Name);
                worksheet1.Cells[j + 1, 9].PutValue(listDocuments[j].TransmittalNumber);
                worksheet1.Cells[j + 1, 10].PutValue(listDocuments[j].ReceivedDate == null ? string.Empty : listDocuments[j].ReceivedDate.Value.ToString("dd/MM/yyyy"));
                worksheet1.Cells[j + 1, 11].PutValue(listDocuments[j].Remark);
                worksheet1.Cells[j + 1, 12].PutValue(listDocuments[j].Well);
            }
            
            worksheet1.AutoFitColumns();
            worksheet1.AutoFitRows();
            Style style = workbook.Styles[workbook.Styles.Add()];
            style.HorizontalAlignment = TextAlignmentType.Center;
            style.VerticalAlignment = TextAlignmentType.Center;
            style.Font.IsBold = true;

            worksheet1.Cells[0, 1].Style = style;
            worksheet1.Cells[0, 2].Style = style;
            worksheet1.Cells[0, 3].Style = style;
            worksheet1.Cells[0, 4].Style = style;
            worksheet1.Cells[0, 5].Style = style;
            worksheet1.Cells[0, 6].Style = style;
            worksheet1.Cells[0, 7].Style = style;
            worksheet1.Cells[0, 8].Style = style;
            worksheet1.Cells[0, 9].Style = style;
            worksheet1.Cells[0, 10].Style = style;
            worksheet1.Cells[0, 11].Style = style;
            worksheet1.Cells[0, 12].Style = style;

            worksheet1.Cells.HideColumn(0);
            workbook.Save(@"C:\Users\Administrator\Desktop\Spire\temp.xls");*/
            #endregion

            var temp = DateTime.ParseExact("14/12/2014", "dd/MM/yyyy", null);
            var tempstr = "5/10/2014 12:00:00";

            var date = tempstr.Substring(0, tempstr.LastIndexOf("/") + 5);

            var temp1 = new DateTime();
            DateTime.TryParseExact("5/31/2014", "M/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out temp1);
            ////for (int i = 1; i <= 14; i++)
            ////{
            ////    var temp = new PermissionWorkgroup()
            ////        {
            ////            UserId = 289,
            ////            PackageId = i,
            ////            ProjectId = 1
            ////        };
            ////    PermissionWorkgroupService.Insert(temp);
            ////    Console.WriteLine("insert " + i);
            ////}

            ////var path = ConfigurationSettings.AppSettings.Get("rootPath");
            ////Console.WriteLine(path);
            ////var fileList = Directory.GetFiles(path);
            ////foreach (var file in fileList)
            ////{
            ////    var fileInfo = new FileInfo(file);
            ////    Console.WriteLine(fileInfo.Name);
            ////    var attachFile = attachFileService.GetByNameServer(fileInfo.Name);
            ////    if (attachFile != null)
            ////    {
            ////        attachFile.FileSize = (double)fileInfo.Length / 1024;
            ////        attachFileService.Update(attachFile);
            ////    }
            ////}

            Console.ReadLine();
        }

        static void CreateValidation(string formular, Validations objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";

            // Set the error message.
            validation.ErrorMessage = "Please select a color from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }


        /// <summary>
        /// The fsw_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.OldName;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var oldFileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var oldPath = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var newFileName = e.Name.Substring(lastPosition + 1, e.Name.Length - lastPosition - 1);

                        var listDocRename = documentService.GetSpecificDocument(oldFileName, oldPath);
                        foreach (var document in listDocRename)
                        {
                            ////var oldRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            document.Name = newFileName;
                            document.FileNameOriginal = newFileName;
                            document.LastUpdatedDate = DateTime.Now;

                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + newFileName;
                            }
                            else
                            {
                                document.RevisionFileName = document.DocIndex + "_" + newFileName;
                            }

                            document.FilePath = document.FilePath.Replace(oldFileName, newFileName);
                            ////document.RevisionFilePath = document.RevisionFilePath.Replace(oldFileName, newFileName);

                            ////var newRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            ////File.Move(oldRevisionPath, newRevisionPath);

                            documentService.Update(document);
                        }

                        ////Console.WriteLine("Renamed: FileName - {0}, ChangeType - {1}, Old FileName - {2}", e.Name, e.ChangeType, e.OldName);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ deleted.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".", StringComparison.Ordinal) + 1, fullPath.Length - fullPath.LastIndexOf(".", StringComparison.Ordinal) - 1);

                if (fileExtensions.Contains(fileExt))
                {
                    try
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var listDocDelete = documentService.GetSpecificDocument(fileName, path);
                        foreach (var document in listDocDelete)
                        {
                            document.IsDelete = true;
                            ////documentService.Update(document);

                            ////var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                   + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;
                            ////var fileTemp = new FileInfo(revisionPath);
                            ////fileTemp.Delete();
                        }

                        Console.WriteLine("Deleted: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// The fsw_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                if (fileExtensions.Contains(fileExt.ToLower()))
                {
                    var lastPosition = fullPath.LastIndexOf(@"\");
                    var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                    var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                    var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);
                    if (objFolder != null)
                    {
                        var docLeaf = documentService.GetSpecificDocument(objFolder.ID, fileName);

                        var objDoc = new Document()
                            {
                                Name = docLeaf.Name,
                                DocumentNumber = docLeaf.DocumentNumber,
                                Title = docLeaf.Title,
                                DocumentTypeID = docLeaf.DocumentTypeID,
                                StatusID = docLeaf.StatusID,
                                DisciplineID = docLeaf.DisciplineID,
                                ReceivedFromID = docLeaf.ReceivedFromID,
                                ReceivedDate = docLeaf.ReceivedDate,
                                TransmittalNumber = docLeaf.TransmittalNumber,
                                LanguageID = docLeaf.LanguageID,
                                Well = docLeaf.Well,
                                Remark = docLeaf.Remark,
                                KeyWords = docLeaf.KeyWords,
                                
                                IsLeaf = true,
                                DocIndex = docLeaf.DocIndex + 1,
                                IsDelete = false,
                                FolderID = docLeaf.FolderID,
                                DirName = docLeaf.DirName,
                                FileExtension = docLeaf.FileExtension,
                                FileExtensionIcon = docLeaf.FileExtensionIcon,
                                FileNameOriginal = docLeaf.FileNameOriginal,
                                FilePath = docLeaf.FilePath,
                                CreatedDate = DateTime.Now
                            };

                        if (docLeaf.ParentID == null)
                        {
                            objDoc.ParentID = docLeaf.ID;
                        }
                        else
                        {
                            objDoc.ParentID = docLeaf.ParentID;
                        }

                        objDoc.RevisionFileName = objDoc.DocIndex + "_" + objDoc.FileNameOriginal;
                        objDoc.RevisionFilePath = docLeaf.RevisionFilePath.Replace(
                            docLeaf.RevisionFileName, objDoc.RevisionFileName);
                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";
                        var tempFile = new FileInfo(e.FullPath);


                        tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);

                        docLeaf.IsLeaf = false;

                        documentService.Update(docLeaf);
                        documentService.Insert(objDoc);
                    }

                    Console.WriteLine("Changed: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                }
            }
        }

        /// <summary>
        /// The fsw_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Created(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 && 
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                if (fileExtensions.Contains(fileExt.ToLower()))
                {
                    try
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);

                        var objDoc = new Document()
                            {
                                Name = fileName,
                                DocIndex = 1,
                                CreatedDate = DateTime.Now,
                                IsLeaf = true,
                                IsDelete = false
                            };
                        objDoc.RevisionFileName = objDoc.DocIndex + "_" + fileName;
                        objDoc.FilePath = "/bdpocedms/DocumentLibrary/" + path + "/" + fileName;
                        objDoc.RevisionFilePath = "/bdpocedms/DocumentLibrary/RevisionHistory/" + objDoc.DocIndex + "_" + fileName;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                       ? fileIcon[fileExt.ToLower()]
                                                       : "images/otherfile.png";
                        objDoc.FileNameOriginal = fileName;
                        objDoc.DirName = "DocumentLibrary/" + path;

                        if (objFolder != null)
                        {
                            objDoc.FolderID = objFolder.ID;
                        }

                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";
                        var tempFile = new FileInfo(e.FullPath);


                        tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);
                        documentService.Insert(objDoc);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException.Message);
                    }

                    Console.WriteLine("Created: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                }
            }
        }

        /// <summary>
        /// The create folder.
        /// </summary>
        static void CreateFolder()
        {
            var rootPath = ConfigurationSettings.AppSettings.Get("rootPath");
            var listFolder = File.ReadAllLines(ConfigurationSettings.AppSettings.Get("listFolder")).ToList();

            ////Directory.CreateDirectory(rootPath + listFolder[1]);
            foreach (var folder in listFolder)
            {
                if (!string.IsNullOrEmpty(folder))
                {
                    Directory.CreateDirectory(rootPath + folder);
                    var listSubfolder = folder.Split('/').Where(t => !string.IsNullOrEmpty(t)).ToList();
                    var dirFather = listSubfolder[0] + "/" + listSubfolder[1];

                    for (int i = 2; i < listSubfolder.Count; i++)
                    {
                        var folFa = folderService.GetByDirName(dirFather);
                        var folChild = folderService.GetByDirName(dirFather + "/" + listSubfolder[i]);
                        if (folChild == null)
                        {
                            folChild = new Folder
                            {
                                Name = listSubfolder[i],
                                DirName = dirFather + "/" + listSubfolder[i],
                                ParentID = folFa.ID,
                                CategoryID = folFa.CategoryID,
                                Description = listSubfolder[i]
                            };

                            folderService.Insert(folChild);
                        }

                        dirFather += "/" + listSubfolder[i];
                    }
                }
            }

        }
    }
}
