﻿using System;
using System.Web;
//using System.Web.Routing;

namespace EDMs.Web
{
    using System.Reflection;

    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            //AuthConfig.RegisterOpenAuth();
            PropertyInfo p = typeof(System.Web.HttpRuntime).GetProperty("FileChangesMonitor", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
            object o = p.GetValue(null, null);
            FieldInfo f = o.GetType().GetField("_dirMonSubdirs", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            object monitor = f.GetValue(o);
            MethodInfo m = monitor.GetType().GetMethod("StopMonitoring", BindingFlags.Instance | BindingFlags.NonPublic);
            m.Invoke(monitor, new object[] { });

            // Start tracking the number of active sessions when the application starts.
            Application.Add("userLoginCount", 0);
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Increase the count of active sessions as they come on.
            int userLoginCount = Convert.ToInt32(Application.Get("userLoginCount").ToString());
            userLoginCount++;

            Application.Set("userLoginCount", userLoginCount);
        }

        void Session_End(object sender, EventArgs e)
        {
            // Decrease the number of active sessions as they end.
            int userLoginCount = Convert.ToInt32(Application.Get("userLoginCount").ToString());
            userLoginCount--;

            Application.Set("userLoginCount", userLoginCount);

           // OnlineActiveUsers.OnlineUsersInstance.OnlineUsers.UpdateForUserLeave();
        }
    }
}
