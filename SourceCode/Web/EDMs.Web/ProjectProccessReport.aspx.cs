﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Data.Entities;
using Telerik.Web.UI.HtmlChart;

namespace EDMs.Web
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;


    using EDMs.Business.Services.Library;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ProjectProccessReport : Page
    {
        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService = new ScopeProjectService();

        private readonly ProcessActualService processActualService = new ProcessActualService();
        private readonly ProcessPlanedService processPlanedService = new ProcessPlanedService();
        private readonly WorkGroupService workGroupService = new WorkGroupService();
        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
                {
                    this.CustomerMenu.Items[1].Visible = false;
                    this.CustomerMenu.Items[2].Visible = false;
                    this.CustomerMenu.Items[3].Visible = false;
                    this.CustomerMenu.Items[4].Visible = false;

                }

                this.InitData();
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);
            }
        }

        protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"Images/project.png";
        }

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.Name).ToList()
                : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0)
                .OrderBy(t => t.Name).ToList();

            this.rtvWorkgroup.DataSource = listWorkgroupInPermission;
            this.rtvWorkgroup.DataTextField = "Name";
            this.rtvWorkgroup.DataValueField = "ID";
            this.rtvWorkgroup.DataFieldID = "ID";
            this.rtvWorkgroup.DataBind();

            this.LoadProcessReport(Convert.ToInt32(this.ddlProject.SelectedValue), 0);
        }

        protected void rtvWorkgroup_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, Convert.ToInt32(e.Node.Value));

        }

        protected void rtvWorkgroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = @"Images/workgroup.png";
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "RefreshProgressReport")
            {
                this.rtvWorkgroup.UnselectAllNodes();
                this.LoadProcessReport(this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0, 0);
            }
            else if (e.Argument == "PrintProgress")
            {
                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var processReportList = new List<ProcessReport>();

                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null && projectObj.StartDate != null && projectObj.EndDate != null)
                {
                    var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                        ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList()
                        : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                            !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0)
                            .OrderBy(t => t.ID).ToList();

                    var dateList = new List<DateTime>();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\ProgressTemplate.xls");
                    
                    var wsProgress = workbook.Worksheets[0];

                    wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                    wsProgress.Cells["C2"].PutValue("PROJECT PROGRESS (CUT OFF: " + DateTime.Now.ToString("dd/MM/yyyy") + ")");

                    var workgroupCount = 0;
                    var startDate = projectObj.StartDate.GetValueOrDefault();
                    //while (startDate.DayOfWeek != DayOfWeek.Monday)
                    //{
                    //    startDate = startDate.AddDays(1);
                    //}

                    var currentMonth = 0;
                    var countMerge = 0;
                    var count = 3;
                    var countNumberCurrentActual = 0;
                    for (var j = startDate;
                            j <= projectObj.EndDate.GetValueOrDefault();
                            j = j.AddDays(7))
                    {
                        var processReport = new ProcessReport();
                        processReport.WeekDate = j;
                        processReportList.Add(processReport);

                        if (DateTime.Now > j)
                        {
                            countNumberCurrentActual += 1;
                        }

                        if (currentMonth != j.AddDays(7).Month)
                        {
                            wsProgress.Cells[2, count].PutValue(j.AddDays(7));
                            currentMonth = j.AddDays(7).Month;

                            if (countMerge != 0)
                            {
                                wsProgress.Cells.Merge(2, count - countMerge, 1, countMerge);
                            }

                            countMerge = 1;
                        }
                        else
                        {
                            countMerge += 1;
                        }

                        wsProgress.Cells[3, count].PutValue(count - 2);
                        wsProgress.Cells[4, count].PutValue(j);
                        //wsProgress.Cells[5, count].PutValue(j.AddDays(5));

                        count += 1;
                    }

                    if (listWorkgroupInPermission.Count > 0)
                    {
                        var wgCount = listWorkgroupInPermission.Count;
                        wsProgress.Cells.InsertRows(8, wgCount * 3);
                        for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                        {
                            var workgroup = listWorkgroupInPermission[i];

                            wsProgress.Cells["B" + (8 + (i * 3))].PutValue(workgroup.Name);
                            wsProgress.Cells["C" + (8 + (i * 3))].PutValue("Planed (%)");
                            wsProgress.Cells["C" + (8 + (i * 3) + 1)].PutValue("Actual (%)");
                            wsProgress.Cells["C" + (8 + (i * 3) + 2)].PutValue("+/-");

                            wsProgress.Cells.Merge(7 + (i*3), 1, 3, 1);

                            var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                            var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);

                            if (progressPlaned != null && progressActual != null)
                            {
                                var planedList = progressPlaned.Planed.Split('$');
                                var actualList = progressActual.Actual.Split('$');
                                for (int j = 0; j < planedList.Count(); j++)
                                {
                                    var planed = !string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0;
                                    var actual = 0.0;

                                    wsProgress.Cells[7 + (i * 3), j + 3].PutValue(planed);
                                    if (j < actualList.Count() && j < countNumberCurrentActual)
                                    {
                                        actual = !string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0;
                                        if (actual != 0.0)
                                        {
                                            wsProgress.Cells[8 + (i * 3), j + 3].PutValue(actual);
                                            wsProgress.Cells[9 + (i * 3), j + 3].PutValue(actual - planed);
                                        }
                                        
                                    }

                                    if (j < processReportList.Count)
                                    {
                                        processReportList[j].Planed += (planed * workgroup.Weight.GetValueOrDefault()) / 100;
                                    }

                                    if (j < actualList.Count() && j < processReportList.Count)
                                    {
                                        processReportList[j].Actual += (Convert.ToDouble(actualList[j])/100 * workgroup.Weight.GetValueOrDefault())/100;
                                    }
                                }
                            }
                        }

                        wsProgress.Cells.Merge(7 + (wgCount * 3), 1, 3, 1);

                        wsProgress.Cells["B" + (8 + (wgCount * 3))].PutValue("Overall Project");
                        wsProgress.Cells["C" + (8 + (wgCount * 3))].PutValue("Planed (%)");
                        wsProgress.Cells["C" + (8 + (wgCount * 3) + 1)].PutValue("Actual (%)");
                        wsProgress.Cells["C" + (8 + (wgCount * 3) + 2)].PutValue("+/-");

                        for (int i = 0; i < processReportList.Count; i++)
                        {
                            wsProgress.Cells[7 + (wgCount * 3), i + 3].PutValue(processReportList[i].Planed);
                            if (processReportList[i].Actual != 0.0)
                            {
                                wsProgress.Cells[8 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual);
                                if (i < countNumberCurrentActual)
                                {
                                    wsProgress.Cells[9 + (wgCount * 3), i + 3].PutValue(processReportList[i].Actual - processReportList[i].Planed);
                                }
                            }
                        }

                        wsProgress.Cells["A1"].PutValue(0);
                        wsProgress.Cells["A2"].PutValue(wgCount);
                        wsProgress.Cells["A3"].PutValue(processReportList.Count);

                        wsProgress.Cells.Merge(0, 2, 1, count - 2);
                        wsProgress.Cells.Merge(1, 2, 1, count - 2);

                    }

                    // Save and export file
                    var filename = (this.ddlProject.SelectedItem != null
                            ? this.ddlProject.SelectedItem.Text + "_"
                            : string.Empty) + "Progress report - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument == "GetLatestData")
            {
                var listDate = new List<DateTime>();
                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null && projectObj.StartDate != null && projectObj.EndDate != null)
                {
                    var count = 0;
                    for (var j = projectObj.StartDate.GetValueOrDefault();
                        j < projectObj.EndDate.GetValueOrDefault();
                        j =  j.AddDays(7))
                    {
                        if (DateTime.Now > j)
                        {
                            count += 1;
                        }
                        
                        //listDate.Add(j);
                    }

                    var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                        ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList()
                        : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                            !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0)
                            .OrderBy(t => t.ID).ToList();
                    foreach (var workgroup in listWorkgroupInPermission)
                    {
                        var docList = this.documentPackageService.GetAllByWorkgroup(workgroup.ID)
                                            .OrderBy(t => t.DocNo)
                                            .ToList();

                        double complete = 0;
                        complete = docList.Aggregate(complete, (current, t) => current + (t.Complete * t.Weight) / 100);

                        var existProgressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                        if (existProgressActual != null)
                        {
                            if (existProgressActual.Actual.Split('$').Count() > count)
                            {
                                var actualList = existProgressActual.Actual.Split('$');
                                actualList[count - 1] = Math.Round(complete,2).ToString();
                                var newActual = string.Empty;
                                newActual = actualList.Aggregate(newActual, (current, t) => current + t + "$");

                                newActual = newActual.Substring(0, newActual.Length - 1);
                                existProgressActual.Actual = newActual;

                                this.processActualService.Update(existProgressActual);
                            }
                        }
                    }

                    this.LoadProcessReport(projectObj.ID, 0);
                }
            }
            else if (e.Argument.Contains("ExportProgress"))
            {
                var type = e.Argument.Split('_')[1];

                var projectId = this.ddlProject.SelectedItem != null
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var projectObj = this.scopeProjectService.GetById(projectId);
                if (projectObj != null && projectObj.StartDate != null && projectObj.EndDate != null)
                {
                    var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                        ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList()
                        : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                            !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0)
                            .OrderBy(t => t.ID).ToList();

                    var dateList = new List<DateTime>();

                    var filePath = Server.MapPath("Exports") + @"\";
                    var workbook = new Workbook();
                    if (type == "Planed")
                    {
                        workbook.Open(filePath + @"Template\ProgressPlanedTemplate.xls");
                    }
                    else
                    {
                        workbook.Open(filePath + @"Template\ProgressActualTemplate.xls");
                    }
                    var wsProgress = workbook.Worksheets[0];

                    wsProgress.Cells["A7"].PutValue(type == "Planed" ? "Planed" : "Actual");
                    wsProgress.Cells["C1"].PutValue("DETAIL ENGINEERING SERVICE FOR " + projectObj.Description);
                    var workgroupCount = 0;
                    var startDate = projectObj.StartDate.GetValueOrDefault();
                    //while (startDate.DayOfWeek != DayOfWeek.Monday)
                    //{
                    //    startDate = startDate.AddDays(1);
                    //}

                    var currentMonth = 0;
                    var countMerge = 0;
                    var count = 3;
                    for (var j = startDate;
                            j <= projectObj.EndDate.GetValueOrDefault();
                            j = j.AddDays(7))
                    {
                        if (currentMonth != j.AddDays(5).Month)
                        {
                            wsProgress.Cells[2, count].PutValue(j.AddDays(5));
                            currentMonth = j.AddDays(5).Month;

                            if (countMerge != 0)
                            {
                                wsProgress.Cells.Merge(2, count - countMerge, 1, countMerge);
                            }

                            countMerge = 1;
                        }
                        else
                        {
                            countMerge += 1;
                        }

                        wsProgress.Cells[3, count].PutValue(count - 2);
                        wsProgress.Cells[4, count].PutValue(j);
                        //wsProgress.Cells[5, count].PutValue(j.AddDays(5));

                        count += 1;
                    }

                    if (listWorkgroupInPermission.Count > 0)
                    {
                        wsProgress.Cells.InsertRows(8, listWorkgroupInPermission.Count);
                        for (int i = 0; i < listWorkgroupInPermission.Count; i++)
                        {
                            var workgroup = listWorkgroupInPermission[i];

                            wsProgress.Cells["A" + (8 + i)].PutValue(workgroup.ID);
                            wsProgress.Cells["B" + (8 + i)].PutValue(workgroup.Name);
                            wsProgress.Cells["C" + (8 + i)].PutValue(type == "Planed" ? "Planed (%)" : "Actual (%)");

                            if (type == "Planed")
                            {
                                var progressPlaned = this.processPlanedService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                if (progressPlaned != null)
                                {
                                    var planedList = progressPlaned.Planed.Split('$');
                                    for (int j = 0; j < planedList.Count(); j++)
                                    {
                                        wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(planedList[j]) ? Convert.ToDouble(planedList[j]) / 100 : 0);
                                    }
                                }
                            }
                            else
                            {
                                var progressActual = this.processActualService.GetByProjectAndWorkgroup(projectObj.ID, workgroup.ID);
                                if (progressActual != null)
                                {
                                    var actualList = progressActual.Actual.Split('$');
                                    for (int j = 0; j < actualList.Count(); j++)
                                    {
                                        wsProgress.Cells[workgroupCount + 7, j + 3].PutValue(!string.IsNullOrEmpty(actualList[j]) ? Convert.ToDouble(actualList[j]) / 100 : 0);
                                    }
                                }
                            }

                            workgroupCount += 1;
                        }

                        wsProgress.Cells.Merge(0, 2, 1, count - 2);
                        wsProgress.Cells.Merge(1, 2, 1, count - 2);
                    }

                    // Save and export file
                    var filename = (this.ddlProject.SelectedItem != null
                            ? this.ddlProject.SelectedItem.Text + "$"
                            : string.Empty)
                            + (type == "Planed" ? "ProgressPlaned.xls" : "ProgressActual.xls");
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
        }
        private void LoadProcessReport(int projectId, int workgroupId)
        {
            var lineSeries = this.LineChart.PlotArea.Series[1] as LineSeries;
            
            var processReportList = new List<ProcessReport>();
            var temp = new List<ProcessReport>();

            var projectObj = this.scopeProjectService.GetById(projectId);
            var processPlanedList = this.processPlanedService.GetAllByProject(projectId, workgroupId);
            var processActualList = this.processActualService.GetAllByProject(projectId, workgroupId);
            if (projectObj != null && projectObj.StartDate != null && projectObj.EndDate != null)
            {
                for (var i = projectObj.StartDate.GetValueOrDefault();
                    i <= projectObj.EndDate.GetValueOrDefault();
                    i = i.AddDays(7))
                {
                    var processReport = new ProcessReport();
                    processReport.WeekDate = i;

                    processReportList.Add(processReport);
                    temp.Add(processReport);
                }

                foreach (var processPlaned in processPlanedList)
                {
                    var count = 0;
                    var actualList = new List<double>();
                    var processActual = processActualList.FirstOrDefault(t => t.WorkgroupId == processPlaned.WorkgroupId);
                    if (processActual != null)
                    {
                        actualList = processActual.Actual.Split('$').Select(Convert.ToDouble).ToList();
                    }

                    var workgroupObj = this.workGroupService.GetById(processPlaned.WorkgroupId.GetValueOrDefault());
                    if (workgroupObj != null)
                    {
                        foreach (var planed in processPlaned.Planed.Split('$').Select(Convert.ToDouble))
                        {
                            if (count < processReportList.Count)
                            {
                                processReportList[count].Planed += workgroupId == 0
                                    ? (planed * workgroupObj.Weight.GetValueOrDefault()) / 100
                                    : planed;
                            }

                            if (count < actualList.Count && count < processReportList.Count)
                            {
                                temp[count].Actual += (workgroupId == 0
                                    ? (actualList[count] * workgroupObj.Weight.GetValueOrDefault()) / 100
                                    : actualList[count]);
                            }

                            count += 1;
                        }
                        
                    }
                }

                this.LineChart.ChartTitle.Text = "DETAIL ENGINEERING SERVICE FOR " + projectObj.Description +
                                                 " | Cut-off: " +
                                                 DateTime.Now.ToString("dd/MM/yyyy");

                
                this.LineChart.DataSource = processReportList;
                this.LineChart.DataBind();
                if (lineSeries != null)
                {
                    lineSeries.Items.Clear();

                    foreach (var actual in temp)
                    {

                        if (actual.Actual == 0)
                        {
                            lineSeries.Items.Add((decimal?) null);
                        }
                        else
                        {
                            lineSeries.Items.Add((decimal?) actual.Actual);
                        }
                    }
                }
            }
            else
            {
                if (projectObj != null)
                {
                    this.LineChart.ChartTitle.Text = "DETAIL ENGINEERING SERVICE FOR " + projectObj.Description +
                                                 " | Cut-off: " +
                                                 DateTime.Now.ToString("dd/MM/yyyy");
                }
                this.LineChart.DataSource = processReportList;
                this.LineChart.DataBind();
            }


        }

        private void InitData()
        {
            var listparent = this.scopeProjectService.GetAll().Where(t => !string.IsNullOrEmpty(t.ParentId.ToString()) && t.ParentId != 0).Select(t => t.ParentId).ToList();
            var projectInPermission = UserSession.Current.User.Id == 1
                ? this.scopeProjectService.GetAll().OrderBy(t => t.Name)
                : this.scopeProjectService.GetAllInPermission(UserSession.Current.User.Id).OrderBy(t => t.Name);
            var project = projectInPermission.Where(t => !listparent.Contains(t.ID)).ToList();
            this.ddlProject.DataSource = project;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var listWorkgroupInPermission = UserSession.Current.User.Id == 1
                ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.Name).ToList()
                : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0)
                .OrderBy(t => t.Name).ToList();

            this.rtvWorkgroup.DataSource = listWorkgroupInPermission;
            this.rtvWorkgroup.DataTextField = "Name";
            this.rtvWorkgroup.DataValueField = "ID";
            this.rtvWorkgroup.DataFieldID = "ID";
            this.rtvWorkgroup.DataBind();
        }

        

        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }

                //Mo file de doc
                var fs = new FileStream(strFileName, FileMode.Open);
                var streamLength = Convert.ToInt32(fs.Length);
                var data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}

