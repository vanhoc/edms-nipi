﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageInfoEditForm : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document new service.
        /// </summary>
        private readonly DocumentNewService documentNewService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ScopeProjectService scopeProjectService;

        /// <summary>
        /// The package service.
        /// </summary>
        private readonly PackageService packageService;

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;
        /// <summary>
        /// the Deparment Service.
        /// </summary>
        private readonly DepartmentService departmentService;
        /// <summary>
        /// the platform service.
        /// </summary>
        private readonly PlatformService platformService;

        private readonly RoleService roleService;
        private readonly WorkGroupService workGroupService;


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageInfoEditForm()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.scopeProjectService = new ScopeProjectService();
            this.packageService = new PackageService();
            this.documentPackageService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.userService = new UserService();
            this.roleService = new RoleService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////this.LoadComboData();
                if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() && !UserSession.Current.User.Role.IsUpdate.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["wgId"]))
                {
                    var selectedWorkGroupId = Convert.ToInt32(this.Request.QueryString["wgId"]);
                    var workGroupObj = this.workGroupService.GetById(selectedWorkGroupId);
                    if (workGroupObj != null)
                    {
                        var projectInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                       ? this.scopeProjectService.GetAll()
                       : this.scopeProjectService.GetAllInPermission(UserSession.Current.User.Id);
                                this.ddlProject.DataSource = projectInPermission;
                                this.ddlProject.DataTextField = "Name";
                                this.ddlProject.DataValueField = "ID";
                                this.ddlProject.DataBind();
                        this.ddlProject.SelectedValue = workGroupObj.ProjectId.ToString();
                        this.ddlWorkgroup.SelectedValue = workGroupObj.ID.ToString();
                        this.LoadComboData();
                    }
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = true;
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {

                        var projectInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                      ? this.scopeProjectService.GetAll()
                      : this.scopeProjectService.GetAllInPermission(UserSession.Current.User.Id);
                        this.ddlProject.DataSource = projectInPermission;
                        this.ddlProject.DataTextField = "Name";
                        this.ddlProject.DataValueField = "ID";
                        this.ddlProject.DataBind();
                        this.ddlProject.SelectedValue = docObj.ProjectId.ToString();

                        this.LoadComboData();

                        this.LoadDocInfo(docObj);

                        var createdUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + docObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (docObj.UpdatedBy != null && docObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(docObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + docObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                DocumentPackage docObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(this.Request.QueryString["docId"]);
                    docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        var oldRevision = docObj.RevisionId;
                        var newRevision = Convert.ToInt32(this.ddlRevision.SelectedValue);
                        if (newRevision > oldRevision)
                        {
                            var docObjNew = new DocumentPackage();
                            this.CollectData(ref docObjNew);

                            // Insert new doc
                            docObjNew.CreatedBy = UserSession.Current.User.Id;
                            docObjNew.CreatedDate = DateTime.Now;
                            docObjNew.IsLeaf = true;
                            docObjNew.ParentId = docObj.ParentId ?? docObj.ID;
                            this.documentPackageService.Insert(docObjNew);

                            // Upate old doc
                            docObj.IsLeaf = false;
                        }
                        else
                        {
                            this.CollectData(ref docObj);
                        }

                        docObj.UpdatedBy = UserSession.Current.User.Id;
                        docObj.UpdatedDate = DateTime.Now;
                        this.documentPackageService.Update(docObj);
                    }
                }
                else
                {
                    docObj = new DocumentPackage()
                    {
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsLeaf = true,
                    };

                    this.CollectData(ref docObj);
                    this.documentPackageService.Insert(docObj);
                }          
                ScriptManager.RegisterStartupScript(this,Page.GetType(), "mykey", "CloseAndRebind();", true);
               
            }
           
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            ////if(this.txtName.Text.Trim().Length == 0)
            ////{
            ////    this.fileNameValidator.ErrorMessage = "Please enter file name.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = false;
            ////}
            ////else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            ////{
            ////    var docId = Convert.ToInt32(Request.QueryString["docId"]);
            ////    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = true; ////!this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            ////}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                
                if(this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            ////var projectInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
            ////   ? this.scopeProjectService.GetAll()
            ////   : this.scopeProjectService.GetAllInPermission(UserSession.Current.User.Id);
            ////this.ddlProject.DataSource = projectInPermission;
            ////this.ddlProject.DataTextField = "Name";
            ////this.ddlProject.DataValueField = "ID";
            ////this.ddlProject.DataBind();

            //var listWorkgroupInPermission = this.workGroupService.GetAllWorkGroupInPermission()

            var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.ID).ToList()
                : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0)
                .OrderBy(t => t.ID).ToList();

            this.ddlWorkgroup.DataSource = listWorkgroupInPermission;
            this.ddlWorkgroup.DataTextField = "Name";
            this.ddlWorkgroup.DataValueField = "ID";
            this.ddlWorkgroup.DataBind();

            var packageList = this.packageService.GetAllPackageOfProject(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            packageList.Insert(0, new Package {ID = 0});
            this.ddlPackage.DataSource = packageList;
            this.ddlPackage.DataTextField = "FullName";
            this.ddlPackage.DataValueField = "ID";
            this.ddlPackage.DataBind();

            var listRevision = this.revisionService.GetAll();
            this.ddlRevision.DataSource = listRevision;
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataBind();

            var listDocumentType = this.documentTypeService.GetAllByCategory(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0).OrderBy(t => t.Name).ToList();
            listDocumentType.Insert(0, new DocumentType() { ID = 0 });
            this.ddlDocType.DataSource = listDocumentType;
            this.ddlDocType.DataTextField = "FullName";
            this.ddlDocType.DataValueField = "ID";
            this.ddlDocType.DataBind();

            var listPlatform = this.platformService.GetAllByProject(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0).OrderBy(t => t.Name).ToList();
            listPlatform.Insert(0, new Platform { ID = 0 });
            var ListPlastform = (from list in listPlatform select new { list.ID, list.Name, FullName = !string.IsNullOrEmpty(list.Description) ? list.Name + "-" + list.Description : string.Empty, list.ProjectId, list.ProjectName, list.CreatedBy, list.CreatedDate, list.LastUpdatedBy, list.LastUpdatedDate }).OrderBy(t => t.Name).ToList();
            this.ddlPlatform.DataSource = ListPlastform;
            this.ddlPlatform.DataTextField = "FullName";
            this.ddlPlatform.DataValueField = "ID";
            this.ddlPlatform.DataBind();

            var listDiscipline = this.disciplineService.GetAllByCategory(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0).OrderBy(t => t.Name).OrderBy(t => t.Name).ToList();
            listDiscipline.Insert(0, new Discipline { ID = 0 });
            this.ddlDiscipline.DataSource = listDiscipline;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();


            var listdepartment = this.departmentService.GetAllByProject(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32
                (this.ddlProject.SelectedValue) : 0).OrderBy(t => t.Name).ToList();
            if (listdepartment.Count > 0)
            {
                listdepartment.Insert(0, new Department { ID = 0 });
                var ListDepartment = (from list in listdepartment select new { list.ID, list.Name, FullName = !string.IsNullOrEmpty(list.Description) ? list.Name + "-" + list.Description : string.Empty, list.ProjectId, list.ProjectName, list.CreatedBy, list.CreatedDate, list.LastUpdatedBy, list.LastUpdatedDate }).OrderBy(t => t.Name).ToList();
                this.ddlDeparment.DataSource = ListDepartment;
                this.ddlDeparment.DataTextField = "FullName";
                this.ddlDeparment.DataValueField = "Id";
                this.ddlDeparment.DataBind();
            }
            else
            { 
            var listDeparment = this.roleService.GetAll(false);
            listDeparment.Insert(0, new Role {Id = 0});
            this.ddlDeparment.DataSource = listDeparment;
            this.ddlDeparment.DataTextField = "FullName";
            this.ddlDeparment.DataValueField = "Id";
            this.ddlDeparment.DataBind();
        }
    }
        private void CollectData(ref DocumentPackage docObj)
        {
            docObj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            docObj.ProjectName = this.ddlProject.SelectedItem.Text;
            docObj.PackageId = Convert.ToInt32(this.ddlPackage.SelectedValue);
            docObj.PackageName = this.ddlPackage.SelectedItem.Text;

            docObj.WorkgroupId = this.ddlWorkgroup.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlWorkgroup.SelectedValue)
                                        : 0;
            docObj.WorkgroupName = this.ddlWorkgroup.SelectedItem != null
                                          ? this.ddlWorkgroup.SelectedItem.Text
                                          : string.Empty;

            docObj.IsEMDR = this.cbIsEMDR.Checked;

            docObj.DocNo = this.txtDocNumber.Text.Trim();
            docObj.DocTitle = this.txtDocumentTitle.Text.Trim();
            docObj.DocumentTypeId = this.ddlDocType.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocType.SelectedValue)
                                        : 0;
            docObj.DocumentTypeName = this.ddlDocType.SelectedItem != null
                                          ? this.ddlDocType.SelectedItem.Text
                                          : string.Empty;
            docObj.PlatformId = this.ddlPlatform.SelectedItem != null ? Convert.ToInt32(this.ddlPlatform.SelectedValue) : 0;
            docObj.PlatformName = this.ddlPlatform.SelectedItem != null ? this.ddlPlatform.SelectedItem.Text : string.Empty;
    

            docObj.DisciplineId = this.ddlDiscipline.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDiscipline.SelectedValue)
                                      : 0;
            docObj.DisciplineName = this.ddlDiscipline.SelectedItem != null
                                        ? this.ddlDiscipline.SelectedItem.Text
                                        : string.Empty;
            docObj.DeparmentId = this.ddlDeparment.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDeparment.SelectedValue)
                                      : 0;
            docObj.DeparmentName = this.ddlDeparment.SelectedItem != null
                                        ? this.ddlDeparment.SelectedItem.Text
                                        : string.Empty;
            docObj.PlatformId = this.ddlPlatform.SelectedItem != null ? Convert.ToInt32(this.ddlPlatform.SelectedValue) : 0;
            docObj.PlatformName = this.ddlPlatform.SelectedItem != null ? this.ddlPlatform.SelectedItem.Text : string.Empty;
            docObj.StartDate = this.txtStartDate.SelectedDate;
            docObj.PlanedDate = this.txtPlanedDate.SelectedDate;
            docObj.Complete = this.txtComplete.Value.GetValueOrDefault();
            docObj.Weight = this.txtWeight.Value.GetValueOrDefault();
            docObj.Notes = this.txtNotes.Text.Trim();

            docObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            docObj.RevisionName = this.ddlRevision.SelectedItem.Text;
            docObj.RevisionPlanedDate = this.txtRevisionPlaned.SelectedDate;
            docObj.RevisionActualDate = this.txtRevisionActual.SelectedDate;
            docObj.RevisionCommentCode = this.txtCommentCode.Text.Trim();

            docObj.OutgoingTransDate = this.txtOutgoingTransDate.SelectedDate;
            docObj.OutgoingTransNo = this.txtOutgoingTransNo.Text.Trim();

            docObj.OutgoingTransXDCBNo = this.txtOutgoingTransXDCBNo.Text.Trim();
            docObj.OutgoingTransXDCBDate = this.txtOutgoingTransXDCBDate.SelectedDate;

            docObj.IncomingTransDate = this.txtIncomingTransDate.SelectedDate;
            docObj.IncomingTransNo = this.txtIncomingTransNo.Text.Trim();

            docObj.ICAReviewCode = this.txtICAReviewCode.Text.Trim();
            docObj.ICAReviewOutTransNo = this.txtICAReviewTransNo.Text.Trim();
            docObj.ICAReviewReceivedDate = this.txtICAReviewDate.SelectedDate;
        }

        private void LoadDocInfo(DocumentPackage docObj)
        {
            this.txtDocNumber.Text = docObj.DocNo;
            this.txtDocumentTitle.Text = docObj.DocTitle;
            this.ddlWorkgroup.SelectedValue = docObj.WorkgroupId.ToString();
            this.ddlDeparment.SelectedValue = docObj.DeparmentId.ToString();
            this.ddlPlatform.SelectedValue = docObj.PlatformId.ToString();
            this.ddlDocType.SelectedValue = docObj.DocumentTypeId.ToString();
            this.ddlDiscipline.SelectedValue = docObj.DisciplineId.ToString();
            this.ddlPackage.SelectedValue = docObj.PackageId.ToString();
            this.txtStartDate.SelectedDate = docObj.StartDate;
            this.txtPlanedDate.SelectedDate = docObj.PlanedDate;
            this.txtComplete.Value = docObj.Complete;
            this.txtWeight.Value = docObj.Weight;
            this.txtNotes.Text = docObj.Notes;

            this.ddlRevision.SelectedValue = docObj.RevisionId.ToString();
            this.txtRevisionPlaned.SelectedDate = docObj.RevisionPlanedDate;
            this.txtRevisionActual.SelectedDate = docObj.RevisionActualDate;
            this.txtCommentCode.Text = docObj.RevisionCommentCode;
            this.ddlPlatform.SelectedValue = docObj.PlatformId.ToString();
            this.txtOutgoingTransNo.Text = docObj.OutgoingTransNo;
            this.txtOutgoingTransDate.SelectedDate = docObj.OutgoingTransDate;

            this.txtOutgoingTransXDCBNo.Text = docObj.OutgoingTransXDCBNo;
            this.txtOutgoingTransXDCBDate.SelectedDate = docObj.OutgoingTransXDCBDate;

            this.txtIncomingTransDate.SelectedDate = docObj.IncomingTransDate;
            this.txtIncomingTransNo.Text = docObj.IncomingTransNo;

            this.txtICAReviewDate.SelectedDate = docObj.ICAReviewReceivedDate;
            this.txtICAReviewTransNo.Text = docObj.ICAReviewOutTransNo;
            this.txtICAReviewCode.Text = docObj.ICAReviewCode;
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue)).OrderBy(t => t.ID).ToList()
                : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0)
                .OrderBy(t => t.ID).ToList();

            this.ddlWorkgroup.DataSource = listWorkgroupInPermission;
            this.ddlWorkgroup.DataTextField = "Name";
            this.ddlWorkgroup.DataValueField = "ID";
            this.ddlWorkgroup.DataBind();

            var packageList = this.packageService.GetAllPackageOfProject(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0);
            packageList.Insert(0, new Package { ID = 0 });
            this.ddlPackage.DataSource = packageList;
            this.ddlPackage.DataTextField = "FullName";
            this.ddlPackage.DataValueField = "ID";
            this.ddlPackage.DataBind();

            var listDocumentType = this.documentTypeService.GetAllByCategory(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0).OrderBy(t => t.Name).ToList();
            listDocumentType.Insert(0, new DocumentType() { ID = 0 });
            this.ddlDocType.DataSource = listDocumentType;
            this.ddlDocType.DataTextField = "FullName";
            this.ddlDocType.DataValueField = "ID";
            this.ddlDocType.DataBind();

            var listDiscipline = this.disciplineService.GetAllByCategory(!string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0).OrderBy(t => t.Name).OrderBy(t => t.Name).ToList();
            listDiscipline.Insert(0, new Discipline { ID = 0 });
            this.ddlDiscipline.DataSource = listDiscipline;
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();
        }
    }
}