﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using OfficeHelper.Utilities.Data;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalAttachDocument : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly TransmittalService transmittalService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DocumentPackageService documentPackageService;

        private readonly WorkGroupService workGroupService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly FolderService folderService;

        private readonly AttachFilesPackageService attachFilesPackageService;

        private readonly TemplateManagementService templateManagementService;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly int TransmittalFolderId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TransFolderId"));

        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public TransmittalAttachDocument()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.receivedFromService = new ReceivedFromService();
            this.transmittalService = new TransmittalService();
            this.categoryService = new CategoryService();
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.documentPackageService = new DocumentPackageService();
            this.workGroupService = new WorkGroupService();
            this.scopeProjectService = new ScopeProjectService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.folderService = new FolderService();
            this.attachFilesPackageService = new AttachFilesPackageService();
            this.templateManagementService = new TemplateManagementService();
            this.userDataPermissionService = new UserDataPermissionService();  
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.Page.IsPostBack)
            {
                this.LoadComboData();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.ddlWorkgroup.Items.Count > 0)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var workgroupId = Convert.ToInt32(this.ddlWorkgroup.SelectedValue);
                var docNo = this.txtDocNo.Text.Trim();
                var docTitle = this.txtDocTitle.Text.Trim();

                var isGetAllRevision = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("GetAllRevisionInTrans"));

                var pageSize = this.grdDocument.PageSize;
                var currentPage = this.grdDocument.CurrentPageIndex;
                var startingRecordNumber = currentPage * pageSize;

                var listDoc = this.documentPackageService.SearchDocument(
                    projectId,
                    workgroupId,
                    docNo,
                    docTitle,
                    string.Empty,
                    isGetAllRevision);
                this.grdDocument.VirtualItemCount = listDoc.Count;
                this.grdDocument.DataSource =
                    listDoc.OrderByDescending(t => t.ID).Skip(startingRecordNumber).Take(pageSize);
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                ? this.scopeProjectService.GetAll()
                : this.scopeProjectService.GetAllInPermission(UserSession.Current.User.Id);

            this.ddlProject.DataSource = projectInPermission;
            this.ddlProject.DataTextField = "Name";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                    .OrderBy(t => t.ID)
                    .ToList()
                : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                    !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                        ? Convert.ToInt32(this.ddlProject.SelectedValue)
                        : 0)
                    .OrderBy(t => t.ID).ToList();
            listWorkgroupInPermission.Insert(0, new WorkGroup {ID =  0});
            this.ddlWorkgroup.DataSource = listWorkgroupInPermission;
            this.ddlWorkgroup.DataTextField = "Name";
            this.ddlWorkgroup.DataValueField = "ID";
            this.ddlWorkgroup.DataBind();
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var objTran = this.transmittalService.GetById(tranId);
                var listSelectedDocId = new List<int>();
                var listAttachFilePackage = new List<AttachFilesPackage>();

                if (objTran != null)
                {
                    var count = 1;
                    var filePath = Server.MapPath("../../Transmittals") + @"\";
                    var workbook = new Workbook();

                    var templateManagement = this.templateManagementService.GetSpecial(4,
                        objTran.ProjectId.GetValueOrDefault());
                    if (templateManagement != null)
                    {
                        // Transmittal for Thien Ung
                        if (templateManagement.ProjectId == 3)
                        {
                            workbook.Open(filePath + templateManagement.FilePath);
                            var sheet1 = workbook.Worksheets[0];

                            var fromUser = this.userService.GetByID(objTran.FromId.GetValueOrDefault());
                            var toUser = this.userService.GetByID(objTran.ToId.GetValueOrDefault());

                            sheet1.Cells["Q5"].PutValue(objTran.TransmittalNumber);
                            sheet1.Cells["G7"].PutValue(fromUser != null ? fromUser.FullName : string.Empty);
                            sheet1.Cells["G8"].PutValue(fromUser != null ? fromUser.Position : string.Empty);
                            sheet1.Cells["G9"].PutValue(fromUser != null ? toUser.FullName : string.Empty);
                            sheet1.Cells["G10"].PutValue(fromUser != null ? toUser.Position : string.Empty);
                            sheet1.Cells["G11"].PutValue(objTran.ReasonForIssue);

                            foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                            {
                                var cboxSelected =
                                    (System.Web.UI.WebControls.CheckBox)
                                        item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                                if (cboxSelected.Checked)
                                {
                                    var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                                    listSelectedDocId.Add(docId);

                                    var attachDoc = new AttachDocToTransmittal()
                                    {
                                        TransmittalId = tranId,
                                        DocumentId = docId
                                    };
                                    if (!this.attachDocToTransmittalService.IsExist(tranId, docId))
                                    {
                                        this.attachDocToTransmittalService.Insert(attachDoc);
                                    }

                                    cboxSelected.Checked = false;
                                }
                            }

                            var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                            sheet1.Cells.InsertRows(19, attachDocList.Count > 0 ? attachDocList.Count - 1 : 0);
                            foreach (var item in attachDocList)
                            {
                                var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                if (docObj != null)
                                {
                                    sheet1.Cells.Merge(17 + count, 2, 1, 6);
                                    sheet1.Cells.Merge(17 + count, 8, 1, 4);

                                    sheet1.Cells[17 + count, 1].PutValue(count);
                                    sheet1.Cells[17 + count, 2].PutValue(docObj.DocNo);
                                    sheet1.Cells[17 + count, 8].PutValue(docObj.DocTitle);
                                    sheet1.Cells["M" + (18 + count).ToString()].PutValue(docObj.RevisionName);
                                    count += 1;

                                    docObj.OutgoingTransNo = objTran.TransmittalNumber;
                                    docObj.OutgoingTransDate = objTran.IssuseDate;

                                    this.documentPackageService.Update(docObj);
                                }


                            }

                            var filename = Utility.RemoveSpecialCharacter(objTran.Name) 
                                            + "_" + DateTime.Now.ToBinary() + ".xls";

                            workbook.Save(filePath + @"Generated\" + filename);

                            var serverPath = "../../Transmittals/Generated/";

                            if (File.Exists(Server.MapPath(objTran.GeneratePath)))
                            {
                                File.Delete(Server.MapPath(objTran.GeneratePath));
                            }

                            objTran.GeneratePath = serverPath + filename;
                            objTran.IsGenerate = true;

                            this.transmittalService.Update(objTran);
                        }
                        else// if (templateManagement.ProjectId == 8 || templateManagement.ProjectId == 9 || templateManagement.ProjectId == 10)
                        {
                            var listTran = new List<Transmittal> { objTran };
                            var transInfo = Utility.ConvertToDataTable(listTran);
                            var docDt = new DataTable();
                            var ds = new DataSet();
                            var listColumn = new DataColumn[]
                            {
                                new DataColumn("Index", Type.GetType("System.String")),
                                new DataColumn("DocumentNumber", Type.GetType("System.String")),
                                new DataColumn("RevisionName", Type.GetType("System.String")),
                                new DataColumn("Description", Type.GetType("System.String")),
                                new DataColumn("Remark", Type.GetType("System.String"))
                            };
                            docDt.Columns.AddRange(listColumn);
                            count = 0;
                            foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                            {
                                var cboxSelected =
                                    (System.Web.UI.WebControls.CheckBox)
                                        item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                                if (cboxSelected.Checked)
                                {
                                    var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                                    var docObj = this.documentPackageService.GetById(docId);
                                    if (docObj != null)
                                    {
                                        listSelectedDocId.Add(docObj.ID);
                                        var attachDoc = new AttachDocToTransmittal()
                                        {
                                            TransmittalId = tranId,
                                            DocumentId = docObj.ID
                                        };

                                        if (!this.attachDocToTransmittalService.IsExist(tranId, docId))
                                        {
                                            this.attachDocToTransmittalService.Insert(attachDoc);
                                        }
                                    }

                                    cboxSelected.Checked = false;
                                }
                            }

                            var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(tranId);
                            foreach (var item in attachDocList)
                            {
                                var docObj = this.documentPackageService.GetById(item.DocumentId.GetValueOrDefault());
                                if (docObj != null)
                                {
                                    var dataItem = docDt.NewRow();
                                    count += 1;
                                    dataItem["Index"] = count;
                                    dataItem["DocumentNumber"] = docObj.DocNo;
                                    dataItem["RevisionName"] = docObj.RevisionName;
                                    dataItem["Description"] = docObj.DocTitle;

                                    dataItem["Remark"] = "1 Bộ";

                                    docDt.Rows.Add(dataItem);

                                    docObj.OutgoingTransNo = objTran.TransmittalNumber;
                                    docObj.OutgoingTransDate = objTran.IssuseDate;

                                    this.documentPackageService.Update(docObj);
                                }
                            }

                            ds.Tables.Add(docDt);
                            ds.Tables[0].TableName = "Table";

                            var rootPath = Server.MapPath("../../Transmittals/");
                            const string WordPath = @"Template\";
                            const string WordPathExport = @"Generated\";
                            var StrTemplateFileName = templateManagement.FilePath;
                            var strOutputFileName = Utility.RemoveSpecialCharacter(objTran.Name) + "_" + DateTime.Now.ToBinary() + ".doc";
                            var isSuccess = OfficeCommon.ExportToWordWithRegion(
                                rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName,
                               transInfo, ds);

                            if (isSuccess)
                            {

                                if (File.Exists(Server.MapPath(objTran.GeneratePath)))
                                {
                                    File.Delete(Server.MapPath(objTran.GeneratePath));
                                }

                                var serverPath = "../../Transmittals/Generated/";
                                objTran.GeneratePath = serverPath + strOutputFileName;
                                objTran.IsGenerate = true;
                                
                                this.transmittalService.Update(objTran);
                            }
                        }




                        // Auto create transmittal folder on Document library and share document files
                        var mainTransFolder = this.folderService.GetById(templateManagement.TransFolderId.GetValueOrDefault());
                        
                        try
                        {
                            var usersInPermissionOfParent = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(mainTransFolder.ID));

                            var transFolderId = 0;
                            var pdfFolderId = 0;
                            var nativeFilesFolderId = 0;

                            // Add new transmittal folder
                            var transfolder =
                                this.folderService.GetByDirName(mainTransFolder.DirName + "/" +
                                            Utility.RemoveSpecialCharacter(objTran.TransmittalNumber));
                            var pdfFolder = new Folder();
                            var nativeFilesFolder = new Folder();
                            if (transfolder == null)
                            {
                                transfolder = new Folder()
                                {
                                    Name = objTran.TransmittalNumber,
                                    ParentID = mainTransFolder.ID,
                                    DirName =
                                        mainTransFolder.DirName + "/" +
                                        Utility.RemoveSpecialCharacter(objTran.TransmittalNumber),
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };

                                Directory.CreateDirectory(Server.MapPath(transfolder.DirName));
                                transFolderId = this.folderService.Insert(transfolder).GetValueOrDefault();

                                // Inherit permission from Parent folder
                                foreach (var parentPermission in usersInPermissionOfParent)
                                {
                                    var childPermission = new UserDataPermission()
                                    {
                                        CategoryId = parentPermission.CategoryId,
                                        RoleId = parentPermission.RoleId,
                                        FolderId = transFolderId,
                                        UserId = parentPermission.UserId,
                                        IsFullPermission = parentPermission.IsFullPermission,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    };

                                    this.userDataPermissionService.Insert(childPermission);
                                }
                                
                            }
                            else
                            {
                                transFolderId = transfolder.ID;
                            }

                            pdfFolder = this.folderService.GetByDirName(transfolder.DirName + "/PDF");
                            nativeFilesFolder = this.folderService.GetByDirName(transfolder.DirName + "/Native Files");

                            if (pdfFolder == null)
                            {
                                pdfFolder = new Folder()
                                {
                                    Name = "PDF",
                                    ParentID = transFolderId,
                                    DirName = transfolder.DirName + "/PDF",
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };
                                Directory.CreateDirectory(Server.MapPath(pdfFolder.DirName));
                                pdfFolderId = this.folderService.Insert(pdfFolder).GetValueOrDefault();

                                // Inherit permission from Parent folder
                                foreach (var parentPermission in usersInPermissionOfParent)
                                {
                                    var childPermission = new UserDataPermission()
                                    {
                                        CategoryId = parentPermission.CategoryId,
                                        RoleId = parentPermission.RoleId,
                                        FolderId = pdfFolderId,
                                        UserId = parentPermission.UserId,
                                        IsFullPermission = parentPermission.IsFullPermission,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    };

                                    this.userDataPermissionService.Insert(childPermission);
                                }
                            }
                            else
                            {
                                pdfFolderId = pdfFolder.ID;
                                ////var existDocList = this.documentService.GetAllByFolder(pdfFolderId);
                                ////foreach (var doc in existDocList)
                                ////{
                                ////    if (File.Exists(Server.MapPath(doc.FilePath)))
                                ////    {
                                ////        File.Delete(Server.MapPath(doc.FilePath));
                                ////    }
                                ////    this.documentService.Delete(doc);
                                ////}

                            }

                            if (nativeFilesFolder == null)
                            {
                                nativeFilesFolder = new Folder()
                                {
                                    Name = "Native Files",
                                    ParentID = transFolderId,
                                    DirName = transfolder.DirName + "/Native Files",
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedDate = DateTime.Now
                                };
                                Directory.CreateDirectory(Server.MapPath(nativeFilesFolder.DirName));
                                nativeFilesFolderId = this.folderService.Insert(nativeFilesFolder).GetValueOrDefault();
                                // Inherit permission from Parent folder
                                foreach (var parentPermission in usersInPermissionOfParent)
                                {
                                    var childPermission = new UserDataPermission()
                                    {
                                        CategoryId = parentPermission.CategoryId,
                                        RoleId = parentPermission.RoleId,
                                        FolderId = nativeFilesFolderId,
                                        UserId = parentPermission.UserId,
                                        IsFullPermission = parentPermission.IsFullPermission,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    };

                                    this.userDataPermissionService.Insert(childPermission);
                                }
                            }
                            else
                            {
                                nativeFilesFolderId = nativeFilesFolder.ID;
                                ////var existDocList = this.documentService.GetAllByFolder(nativeFilesFolderId);
                                ////foreach (var doc in existDocList)
                                ////{
                                ////    if (File.Exists(Server.MapPath(doc.FilePath)))
                                ////    {
                                ////        File.Delete(Server.MapPath(doc.FilePath));
                                ////    }
                                ////    this.documentService.Delete(doc);
                                ////}
                            }

                            foreach (var docId in listSelectedDocId)
                            {
                                listAttachFilePackage.AddRange(
                                    this.attachFilesPackageService.GetAllDocumentFileByDocId(docId));
                            }

                            var pdfFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() == "pdf").ToList();
                            var nativeFiles = listAttachFilePackage.Where(t => t.Extension.ToLower() != "pdf").ToList();

                            foreach (var pdfFile in pdfFiles)
                            {
                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(pdfFolder.DirName), pdfFile.FileName);
                                // Path file to download from server
                                var serverFilePath = pdfFolder.DirName + "/" + pdfFile.FileName;
                                var fileExt = pdfFile.Extension;

                                if (!File.Exists(saveFilePath))
                                {
                                    var document = new Document()
                                    {
                                        Name = pdfFile.FileName,
                                        FileExtension = fileExt,
                                        FileExtensionIcon = pdfFile.ExtensionIcon,
                                        FilePath = serverFilePath,
                                        FolderID = pdfFolderId,
                                        IsLeaf = true,
                                        IsDelete = false,
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };
                                    this.documentService.Insert(document);
                                }
                                
                                if (File.Exists(Server.MapPath(pdfFile.FilePath)))
                                {
                                    File.Copy(Server.MapPath(pdfFile.FilePath), saveFilePath, true);
                                }

                                
                            }

                            foreach (var nativeFile in nativeFiles)
                            {
                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(nativeFilesFolder.DirName),
                                    nativeFile.FileName);
                                // Path file to download from server
                                var serverFilePath = nativeFilesFolder.DirName + "/" + nativeFile.FileName;
                                var fileExt = nativeFile.Extension;

                                if (!File.Exists(saveFilePath))
                                {
                                    var document = new Document()
                                    {
                                        Name = nativeFile.FileName,
                                        FileExtension = fileExt,
                                        FileExtensionIcon = nativeFile.ExtensionIcon,
                                        FilePath = serverFilePath,
                                        FolderID = nativeFilesFolderId,
                                        IsLeaf = true,
                                        IsDelete = false,
                                        CreatedBy = UserSession.Current.User.Id,
                                        CreatedDate = DateTime.Now
                                    };
                                    this.documentService.Insert(document);
                                }

                                if (File.Exists(Server.MapPath(nativeFile.FilePath)))
                                {
                                    File.Copy(Server.MapPath(nativeFile.FilePath), saveFilePath, true);
                                }
                            }

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }

            ////this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);

        }

        protected void ddlProject_SelectedIndexChange(object sender, EventArgs e)
        {
            var listWorkgroupInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
             ? this.workGroupService.GetAllWorkGroupOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                 .OrderBy(t => t.ID)
                 .ToList()
             : this.workGroupService.GetAllWorkGroupInPermission(UserSession.Current.User.Id,
                 !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                     ? Convert.ToInt32(this.ddlProject.SelectedValue)
                     : 0)
                 .OrderBy(t => t.ID).ToList();
            listWorkgroupInPermission.Insert(0, new WorkGroup {ID = 0});
            this.ddlWorkgroup.DataSource = listWorkgroupInPermission;
            this.ddlWorkgroup.DataTextField = "Name";
            this.ddlWorkgroup.DataValueField = "ID";
            this.ddlWorkgroup.DataBind();

            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (item["HasAttachFile"].Text == "True")
                {
                    item.BackColor = Color.Aqua;
                    item.BorderColor = Color.Aqua;
                }
            }
        }
    }
}