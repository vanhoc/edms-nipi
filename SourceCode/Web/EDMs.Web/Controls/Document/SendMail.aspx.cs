﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Web.Utilities.Sessions;
    using EDMs.Web.Utilities;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class SendMail : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The document new service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public SendMail()
        {
            this.documentService = new DocumentService();
            this.userService = new UserService();
            this.documentPackageService = new DocumentPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                this.ddlEmail.Focus();

                if (!string.IsNullOrEmpty(this.Request.QueryString["listDoc"]))
                {
                    var count = 0;
                    var listDocId =
                        this.Request.QueryString["listDoc"].Split(',').Where(t => !string.IsNullOrEmpty(t)).Select(
                            t => Convert.ToInt32(t));
                    var bodyContent = @"******<br/>
                                        Dear users,<br/><br/>

                                        Please be informed that the following documents are now available on the SPF System for your information.<br/><br/>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:350px'>Name</th>
		                                        <th style='text-align:center; width:350px'>Description</th>
                                                <th style='text-align:center; width:70px'>Rev</th>
	                                        </tr>";
                    foreach (var docId in listDocId)
                    {
                        var document = this.documentPackageService.GetById(docId);
                        var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                        if (document != null)
                        {
                            count += 1;
                            bodyContent += @"<tr>
                                <td style='text-align: center;'>" + count + @"</td>
                                <td>" +  document.DocNo + @"</td>" +
                                "<td>" + document.DocTitle + @"</td>" +
                                "<td>" + document.RevisionName + @"</td>" +
                                "</tr>";
                        }
                    }

                    bodyContent += @"</table>
                                        <br/><br/>
                                        Thanks and regards,<br/>
                                        ******";

                    this.txtEmailBody.Content = bodyContent;
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var listUser = this.userService.GetAll().Where(t => !string.IsNullOrEmpty(t.Email));
            this.ddlEmail.DataSource = listUser;
            this.ddlEmail.DataBind();
        }

        protected void SendMailMenu_OnButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (this.IsValid)
            {
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(UserSession.Current.User.Email, Utility.Decrypt(UserSession.Current.User.HashCode))
                };

                var message = new MailMessage();
                message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                message.Subject = this.txtSubject.Text.Trim();
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;
                message.Body = this.txtEmailBody.Content;

                if (!string.IsNullOrEmpty(this.ddlEmail.Text))
                {
                    var toList = this.ddlEmail.Text.Split(',').Where(t => !string.IsNullOrEmpty(t));
                    foreach (var to in toList)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    smtpClient.Send(message);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "Close();", true);
            }
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationEmptyEmailAddress(object source, ServerValidateEventArgs args)
        {
            if (this.ddlEmail.Text.Trim().Length == 0)
            {
                this.selectEmailValidate.ErrorMessage = "Please enter email address.";
                args.IsValid = false;
            }
        }
    }
}