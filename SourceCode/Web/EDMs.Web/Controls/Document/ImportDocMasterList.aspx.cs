﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Globalization;
using System.Runtime.InteropServices;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DocumentNewService documentNewService;

        private readonly CategoryService categoryService;

        private readonly OptionalTypeDetailService optionalTypeDetailService;

        private readonly OriginatorService originatorService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportDocMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.documentNewService = new DocumentNewService();
            this.categoryService = new CategoryService();
            this.optionalTypeDetailService = new OptionalTypeDetailService();
            this.originatorService = new OriginatorService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var currentValue = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var project = this.scopeProjectService.GetByName(docFile.FileName.Split('$')[0]);
                        if (project != null)
                        {
                            if (project.Name == "RC9RC5")
                            {


                                var isAutoDetectDocNo =
                                    Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AutoDetectDocNo"));
                                // Instantiate a new workbook
                                var workbook = new Workbook();
                                workbook.Open(importPath);

                                foreach (Worksheet worksheet in workbook.Worksheets)
                                {
                                    var currentVersion = worksheet.Cells["A2"].Value;

                                    if (currentVersion != null && currentVersion.ToString() == ConfigurationManager.AppSettings.Get("CurrentMasterFileVersion"))
                                    {
                                        var workgroupId = worksheet.Cells["A1"].Value;

                                        var workgroup = this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                                        currentSheetName = worksheet.Name;
                                        currentDocumentNo = string.Empty;

                                        if (workgroup != null)
                                        {

                                            // Create a datatable
                                            var dataTable = new DataTable();

                                            // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                            dataTable = worksheet.Cells.ExportDataTable(2, 1,
                                                worksheet.Cells.MaxRow, 24);

                                            foreach (DataRow dataRow in dataTable.Rows)
                                            {
                                                if (!string.IsNullOrEmpty(dataRow["Column3"].ToString()))
                                                {
                                                    currentDocumentNo = dataRow["Column2"].ToString();

                                                    var package = new Package();
                                                    var discipline = new Discipline();
                                                    var documentType = new DocumentType();
                                                    var department = new Department();
                                                    var platform= new Platform();                                                 

                                                    var docNo = dataRow["Column2"].ToString().Replace(" ", string.Empty);
                                                    var detectData = docNo.Split('-').Select(t => t.Trim()).ToList();
                                                    if (detectData[0].ToString().Trim() == "RC9RC5")
                                                    {
                                                        if (detectData[2].Length > 3)
                                                        {
                                                            var platformname = detectData[2].Substring(0, 3);
                                                            platform = this.platformService.GetByName(platformname, project.ID);
                                                            var packagename = detectData[2].Substring(platformname.Length, detectData[2].Length - platformname.Length);
                                                            package = this.packageService.GetByName(packagename, project.ID);
                                                        }
                                                        else
                                                        {
                                                            platform = this.platformService.GetByName(detectData[2], project.ID);
                                                            package = this.packageService.GetByName(detectData[2],project.ID);
                                                        }


                                                        var departmentname = detectData[3].Substring(detectData[3].Length - 1, 1);
                                                        var disciplinename = detectData[3].Substring(0, detectData[3].Length - 1);

                                                        if (detectData[3].Contains("10"))
                                                        {

                                                            departmentname = detectData[3].Substring(detectData[3].Length - 2, 2);
                                                            department = this.departmentService.GetByName(departmentname, project.ID);
                                                            disciplinename = detectData[3].Substring(0, detectData[3].Length - 2);
                                                            discipline =
                                                           this.disciplineService.GetByName(disciplinename, project.ID);

                                                        }
                                                        else
                                                        {
                                                            department = this.departmentService.GetByName(departmentname, project.ID);
                                                            if (department != null)
                                                            {
                                                                discipline =
                                                                    this.disciplineService.GetByName(disciplinename, project.ID);
                                                            }
                                                            else
                                                            {
                                                                discipline =
                                                                   this.disciplineService.GetByName(detectData[3], project.ID);
                                                            }
                                                        }
                                                        
                                                       
                                                        documentType =
                                                            this.documentTypeService.GetByName(detectData[4], project.ID);
                                                    }
                                                    else
                                                    {
                                                        package = this.packageService.GetByName(detectData[2], project.ID);
                                                        var departmentname = detectData[3].Substring(detectData[3].Length - 1, 1);
                                                        var disciplinename = detectData[3].Substring(0, detectData[3].Length - 1);
                                                        platform = this.platformService.GetByName(detectData[1],project.ID);
                                                        if (detectData[3].Contains("10"))
                                                        {

                                                            departmentname = detectData[3].Substring(detectData[3].Length - 2, 2);
                                                            department = this.departmentService.GetByName(departmentname, project.ID);
                                                            disciplinename = detectData[3].Substring(0, detectData[3].Length - 2);
                                                            discipline =
                                                           this.disciplineService.GetByName(disciplinename, project.ID);

                                                        }
                                                        else
                                                        {
                                                            department = this.departmentService.GetByName(departmentname, project.ID);
                                                            if (department != null)
                                                            {
                                                                discipline =
                                                                    this.disciplineService.GetByName(disciplinename, project.ID);
                                                            }
                                                            else
                                                            {
                                                                discipline =
                                                                   this.disciplineService.GetByName(detectData[3], project.ID);
                                                            }
                                                        }
                                                        
                                                        documentType =
                                                          this.documentTypeService.GetByName(detectData[4], project.ID);
                                                            
                                                    }  
                                              
                                                 
                                                    var revision = this.revisionService.GetByName(dataRow["Column5"].ToString());

                                                    var docObj = new DocumentPackage();
                                                  
                                                    
                                                    docObj.WorkgroupId = workgroup.ID;
                                                    docObj.WorkgroupName = workgroup.Name;

                                                    docObj.DocNo = dataRow["Column2"].ToString().Replace(" ", string.Empty);
                                                    docObj.DocTitle = dataRow["Column3"].ToString();

                                                    docObj.PackageId = package != null ? package.ID : 0;
                                                    docObj.PackageName = package != null ? package.Name : "";

                                                    docObj.DisciplineId = discipline != null ? discipline.ID : 0;
                                                    docObj.DisciplineName = discipline != null ? discipline.Name : "";

                                                    docObj.DocumentTypeId = documentType != null ? documentType.ID : 0;
                                                    docObj.DocumentTypeName = documentType != null
                                                        ? documentType.FullName
                                                        : string.Empty;

                                                    docObj.DeparmentId = department != null ? department.ID : 0;
                                                    docObj.DeparmentName = department != null ? department.Name : "";
                                                   
                                                    docObj.PlatformId = platform != null ? platform.ID : 0;
                                                    docObj.PlatformName = platform != null ? platform.Name : "";

                                                    docObj.ProjectId = project.ID;
                                                    docObj.ProjectName = project.Name;

                                                    var strstartDate = dataRow["Column4"].ToString();
                                                    var StartDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strstartDate, ref StartDate))
                                                    {
                                                        docObj.StartDate = StartDate;
                                                    }

                                                    docObj.RevisionId = revision != null ? revision.ID : 0;
                                                    docObj.RevisionName = revision != null ? revision.Name : "";

                                                    var strRevPlanedDate = dataRow["Column6"].ToString();
                                                    var RevPlanedDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strRevPlanedDate, ref RevPlanedDate))
                                                    {
                                                        docObj.RevisionPlanedDate = RevPlanedDate;
                                                    }

                                                    var strRevActualDate = dataRow["Column7"].ToString();
                                                    var RevActualDate = new DateTime();
                                                    if (Utility.ConvertStringToDateTime(strRevActualDate, ref RevActualDate))
                                                    {
                                                        docObj.RevisionActualDate = RevActualDate;
                                                    }

                                                    docObj.Complete = !string.IsNullOrEmpty(dataRow["Column8"].ToString())
                                                        ? Math.Round(Convert.ToDouble(dataRow["Column8"]) * 100, 2)
                                                        : 0;
                                                    docObj.Weight = !string.IsNullOrEmpty(dataRow["Column9"].ToString())
                                                        ? Math.Round(Convert.ToDouble(dataRow["Column9"]) * 100, 2)
                                                        : 0;

                                                    docObj.OutgoingTransNo = string.Empty;
                                                    docObj.IncomingTransNo = string.Empty;
                                                    docObj.ICAReviewCode = string.Empty;
                                                    docObj.RevisionCommentCode = string.Empty;
                                                    docObj.ICAReviewOutTransNo = string.Empty;

                                                    docObj.Notes = dataRow["Column11"].ToString();
                                                    docObj.IsEMDR = dataRow["Column12"].ToString().ToLower() == "x";

                                                    docObj.IsLeaf = true;
                                                    docObj.IsDelete = false;
                                                    docObj.CreatedBy = UserSession.Current.User.Id;
                                                    docObj.CreatedDate = DateTime.Now;

                                                    documentPackageList.Add(docObj);

                                                    if (!this.cbCheckValidFile.Checked)
                                                    {
                                                        this.documentPackageService.Insert(docObj);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //this.blockError.Visible = true;
                                            //this.lblError.Text = "Ducument master list file is invalid. Please re-export new template file to input data.";

                                        }

                                        if (this.cbCheckValidFile.Checked)
                                        {
                                            foreach (var documentPackage in documentPackageList)
                                            {
                                                this.documentPackageService.Insert(documentPackage);
                                            }

                                            this.blockError.Visible = true;
                                            this.lblError.Text = "Data of document master list file is valid.";
                                        }
                                        else
                                        {
                                            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                                        }
                                    }
                                    else
                                    {
                                        this.blockError.Visible = true;
                                        this.lblError.Text = "The document master list file is old. Please export the latest template from system.";
                                        break;
                                    }
                                }
                            }
                            else
                            {

                            
                            var isAutoDetectDocNo =
                                Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AutoDetectDocNo"));
                            // Instantiate a new workbook
                            var workbook = new Workbook();
                            workbook.Open(importPath);

                            foreach (Worksheet worksheet in workbook.Worksheets)
                            {
                                var currentVersion = worksheet.Cells["A2"].Value;

                                if (currentVersion != null && currentVersion.ToString() == ConfigurationManager.AppSettings.Get("CurrentMasterFileVersion"))
                                {
                                    var workgroupId = worksheet.Cells["A1"].Value;

                                    var workgroup = this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                                    currentSheetName = worksheet.Name;
                                    currentDocumentNo = string.Empty;

                                    if (workgroup != null)
                                    {
                                       
                                        // Create a datatable
                                        var dataTable = new DataTable();

                                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                        dataTable = worksheet.Cells.ExportDataTable(2, 1,
                                            worksheet.Cells.MaxRow, 24);

                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            if (!string.IsNullOrEmpty(dataRow["Column3"].ToString()))
                                            {
                                                currentDocumentNo = dataRow["Column2"].ToString();

                                                var package = new Package();
                                                var discipline = new Discipline();
                                                var documentType = new DocumentType();
                                                var platform = new Platform();

                                                if (isAutoDetectDocNo)
                                                {
                                                    var docNo = dataRow["Column2"].ToString().Replace(" ", string.Empty);
                                                    var detectData = docNo.Split('-').Select(t => t.Trim()).ToList();

                                                    package = this.packageService.GetByName(detectData[2], workgroup.ProjectId.GetValueOrDefault());
                                                    discipline =
                                                        this.disciplineService.GetByName(detectData[3], workgroup.ProjectId.GetValueOrDefault());
                                                    documentType =
                                                        this.documentTypeService.GetByName(detectData[4], workgroup.ProjectId.GetValueOrDefault());
                                                    platform = this.platformService.GetByName(detectData[1],workgroup.ProjectId.GetValueOrDefault());
                                                }

                                                var department = this.roleService.GetByName(dataRow["Column10"].ToString());
                                                var revision = this.revisionService.GetByName(dataRow["Column5"].ToString());
                                                
                                                var docObj = new DocumentPackage();
                                                ////docObj.IndexNumber = !string.IsNullOrEmpty(dataRow["Column1"].ToString())
                                                ////    ? Convert.ToInt32(dataRow["Column1"])
                                                ////    : 0;

                                                docObj.WorkgroupId = workgroup.ID;
                                                docObj.WorkgroupName = workgroup.Name;

                                                docObj.DocNo = dataRow["Column2"].ToString().Replace(" ", string.Empty);
                                                docObj.DocTitle = dataRow["Column3"].ToString();

                                                docObj.PackageId = package != null ? package.ID : 0;
                                                docObj.PackageName = package != null ? package.Name : string.Empty;

                                                docObj.DisciplineId = discipline != null ? discipline.ID : 0;
                                                docObj.DisciplineName = discipline != null ? discipline.Name : string.Empty;

                                                docObj.DocumentTypeId = documentType != null ? documentType.ID : 0;
                                                docObj.DocumentTypeName = documentType != null
                                                    ? documentType.FullName
                                                    : string.Empty;

                                                docObj.DeparmentId = department != null ? department.Id : 0;
                                                docObj.DeparmentName = department != null ? department.Name : string.Empty;

                                                docObj.PlatformId = platform != null ? platform.ID : 0;
                                                docObj.PlatformName = platform != null ? platform.Name : string.Empty;

                                                docObj.ProjectId = project.ID;
                                                docObj.ProjectName = project.Name;

                                                var strstartDate = dataRow["Column4"].ToString();
                                                var StartDate = new DateTime();
                                                if (Utility.ConvertStringToDateTime(strstartDate, ref StartDate))
                                                {
                                                    docObj.StartDate = StartDate;
                                                }

                                                docObj.RevisionId = revision != null ? revision.ID : 0;
                                                docObj.RevisionName = revision != null ? revision.Name : string.Empty;

                                                var strRevPlanedDate = dataRow["Column6"].ToString();
                                                var RevPlanedDate = new DateTime();
                                                if (Utility.ConvertStringToDateTime(strRevPlanedDate, ref RevPlanedDate))
                                                {
                                                    docObj.RevisionPlanedDate = RevPlanedDate;
                                                }

                                                var strRevActualDate = dataRow["Column7"].ToString();
                                                var RevActualDate = new DateTime();
                                                if (Utility.ConvertStringToDateTime(strRevActualDate, ref RevActualDate))
                                                {
                                                    docObj.RevisionActualDate = RevActualDate;
                                                }

                                                docObj.Complete = !string.IsNullOrEmpty(dataRow["Column8"].ToString())
                                                    ? Math.Round(Convert.ToDouble(dataRow["Column8"]) * 100, 2)
                                                    : 0;
                                                docObj.Weight = !string.IsNullOrEmpty(dataRow["Column9"].ToString())
                                                    ? Math.Round(Convert.ToDouble(dataRow["Column9"]) * 100, 2)
                                                    : 0;

                                                docObj.OutgoingTransNo = string.Empty;
                                                docObj.IncomingTransNo = string.Empty;
                                                docObj.ICAReviewCode = string.Empty;
                                                docObj.RevisionCommentCode = string.Empty;
                                                docObj.ICAReviewOutTransNo = string.Empty;

                                                docObj.Notes = dataRow["Column11"].ToString();
                                                docObj.IsEMDR = dataRow["Column12"].ToString().ToLower() == "x";

                                                docObj.IsLeaf = true;
                                                docObj.IsDelete = false;
                                                docObj.CreatedBy = UserSession.Current.User.Id;
                                                docObj.CreatedDate = DateTime.Now;

                                                documentPackageList.Add(docObj);

                                                if (!this.cbCheckValidFile.Checked)
                                                {
                                                   this.documentPackageService.Insert(docObj);
                                                 
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //this.blockError.Visible = true;
                                        //this.lblError.Text = "Ducument master list file is invalid. Please re-export new template file to input data.";

                                    }

                                    if (this.cbCheckValidFile.Checked)
                                    {
                                        foreach (var documentPackage in documentPackageList)
                                        {
                                            this.documentPackageService.Insert(documentPackage);
                                        }

                                        this.blockError.Visible = true;
                                        this.lblError.Text = "Data of document master list file is valid.";
                                    }
                                    else
                                    {
                                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                                    }
                                }
                                else
                                {
                                    this.blockError.Visible = true;
                                    this.lblError.Text = "The document master list file is old. Please export the latest template from system.";
                                    break;
                                }
                            }
                        }}
                    }
                }
           }
           catch (Exception ex)
           {
               this.blockError.Visible = true;
               this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
           }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}