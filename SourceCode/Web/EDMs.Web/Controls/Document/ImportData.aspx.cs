﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportData : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly TransmittalService transmittalService;

        private readonly ScopeProjectService scopeProjectService;

        private readonly WorkGroupService workGroupService;

        private readonly ProcessPlanedService processPlanedService;
        private readonly ProcessActualService processActualService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportData()
        {
            this.documentService = new DocumentService();
            this.transmittalService = new TransmittalService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.processPlanedService = new ProcessPlanedService();
            this.processActualService = new ProcessActualService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    var transID = Convert.ToInt32(this.Request.QueryString["tranId"]);
                    var transObj = this.transmittalService.GetById(transID);

                    if (transObj != null)
                    {
                        foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                        {
                            var filename = DateTime.Now.ToBinary() + "_" + docFile.FileName;
                            var oldfilePath = Server.MapPath(transObj.GeneratePath);

                            var newServerPath = "../../Transmittals/Generated/" + filename;
                            var newFilePath = Server.MapPath("../../Transmittals/Generated/") + filename;

                            docFile.SaveAs(newFilePath);

                            transObj.GeneratePath = newServerPath;
                            transObj.LastUpdatedBy = UserSession.Current.User.Id;
                            transObj.LastUpdatedDate = DateTime.Now;

                            if (File.Exists(oldfilePath))
                            {
                                File.Delete(oldfilePath);
                            }

                            this.transmittalService.Update(transObj);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(this.Request.QueryString["type"]))
                {
                    var type = this.Request.QueryString["type"];
                    switch (type)
                    {
                        case "progress":
                            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                            {
                                var extension = docFile.GetExtension();
                                if (extension == ".xls" || extension == ".xlsx")
                                {
                                    var importPath = Server.MapPath("../../Import") + "/" +
                                                     DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                                     "_" + docFile.FileName;
                                    docFile.SaveAs(importPath);

                                    var project = this.scopeProjectService.GetByName(docFile.FileName.Split('$')[0]);
                                    if (project != null && project.StartDate != null && project.EndDate != null)
                                    {
                                        var count = 0;
                                        var startDate = project.StartDate.GetValueOrDefault();
                                        //while (startDate.DayOfWeek != DayOfWeek.Monday)
                                        //{
                                        //    startDate = startDate.AddDays(1);
                                        //}

                                        for (var j = startDate;
                                            j <= project.EndDate.GetValueOrDefault();
                                            j = j.AddDays(7))
                                        {
                                            count += 1;
                                        }

                                        var workbook = new Workbook();
                                        workbook.Open(importPath);

                                        var wsProgress = workbook.Worksheets[0];

                                        var progressType = wsProgress.Cells["A7"].Value.ToString();
                                        if (progressType == "Planed" || progressType == "Actual")
                                        {
                                            // Create a datatable
                                            var dataTable = new DataTable();

                                            // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                                            dataTable = wsProgress.Cells.ExportDataTable(7, 0,
                                                wsProgress.Cells.MaxRow - 7, count + 3);

                                            foreach (DataRow dataRow in dataTable.Rows)
                                            {
                                                var workgroupId = dataRow["Column1"].ToString();
                                                if (!string.IsNullOrEmpty(workgroupId))
                                                {
                                                    var workgroup =
                                                        this.workGroupService.GetById(Convert.ToInt32(workgroupId));
                                                    if (workgroup != null)
                                                    {
                                                        var progressValue = string.Empty;
                                                        for (int i = 0; i < count; i++)
                                                        {
                                                            var value =
                                                                !string.IsNullOrEmpty(
                                                                    dataRow["Column" + (i + 4)].ToString())
                                                                    ? Math.Round(Convert.ToDouble(
                                                                        dataRow["Column" + (i + 4)].ToString()), 4)*100
                                                                    : 0;
                                                            progressValue += value + "$";

                                                        }

                                                        progressValue = progressValue.Substring(0, progressValue.Length - 1);

                                                        if (progressType == "Planed")
                                                        {
                                                            var existProgressPlaned =
                                                                this.processPlanedService.GetByProjectAndWorkgroup(
                                                                    project.ID, workgroup.ID);

                                                            if (existProgressPlaned == null)
                                                            {
                                                                var progressPlaned = new ProcessPlaned();
                                                                progressPlaned.ProjectId = workgroup.ProjectId;
                                                                progressPlaned.WorkgroupId = workgroup.ID;
                                                                progressPlaned.Planed = progressValue;

                                                                this.processPlanedService.Insert(progressPlaned);
                                                            }
                                                            else
                                                            {
                                                                existProgressPlaned.Planed = progressValue;

                                                                this.processPlanedService.Update(existProgressPlaned);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            var existProgressActual =
                                                                this.processActualService.GetByProjectAndWorkgroup(
                                                                    project.ID, workgroup.ID);

                                                            if (existProgressActual == null)
                                                            {
                                                                var progressActual = new ProcessActual();
                                                                progressActual.ProjectId = workgroup.ProjectId;
                                                                progressActual.WorkgroupId = workgroup.ID;
                                                                progressActual.Actual = progressValue;

                                                                this.processActualService.Insert(progressActual);
                                                            }
                                                            else
                                                            {
                                                                existProgressActual.Actual = progressValue;

                                                                this.processActualService.Update(existProgressActual);
                                                            }
                                                        }
                                                        
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            this.blockError.Visible = true;
                                            this.lblError.Text = "This file is invalid";
                                        }
                                        

                                    }
                                }
                            }

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error when import Progress: <br/>'" + ex.Message + "'";
            }

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    docFile.SaveAs(revisionFilePath, true);
                }
            }
        }
    }
}