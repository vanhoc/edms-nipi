﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Scope
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Controls.Document;
    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ScopeProjectEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly ScopeProjectService ScopeProjectService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ScopeProjectEditForm()
        {
            this.userService = new UserService();
            this.ScopeProjectService = new ScopeProjectService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService = new CategoryService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                var listParent = this.ScopeProjectService.GetAll().Where(t => t.ParentId == null || t.ParentId == 0).ToList(); ;
                listParent.Insert(0, new ScopeProject {ID=0,Name=null});
                this.ddlParent.DataSource = listParent;
                this.ddlParent.DataTextField = "Name";
                this.ddlParent.DataValueField = "ID";
                this.ddlParent.DataBind();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var objScopeProject = this.ScopeProjectService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objScopeProject != null)
                    {
                        this.txtName.Text = objScopeProject.Name;
                        this.txtDescription.Text = objScopeProject.Description;
                        this.txtStartDate.SelectedDate = objScopeProject.StartDate;
                        this.txtEndDate.SelectedDate = objScopeProject.EndDate;
                        this.ddlParent.SelectedValue = objScopeProject.ParentId.ToString();
                    }

                     listParent = listParent.Where(t => t.ID != Convert.ToInt32(this.Request.QueryString["disId"])).ToList();
                    this.ddlParent.DataSource = listParent;
                    this.ddlParent.DataTextField = "Name";
                    this.ddlParent.DataValueField = "ID";
                    this.ddlParent.DataBind();
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var ScopeProjectId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.ScopeProjectService.GetById(ScopeProjectId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.StartDate = this.txtStartDate.SelectedDate;
                        obj.EndDate = this.txtEndDate.SelectedDate;
                        obj.ParentId = Convert.ToInt32(this.ddlParent.SelectedValue);
                        this.ScopeProjectService.Update(obj);
                    }
                }
                else
                {
                    var obj = new ScopeProject()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        StartDate = this.txtStartDate.SelectedDate,
                        EndDate = this.txtEndDate.SelectedDate,
                        ParentId = Convert.ToInt32(this.ddlParent.SelectedValue)
                    };

                    this.ScopeProjectService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }
    }
}