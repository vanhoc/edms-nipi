﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web
{
    using System.Configuration;

    using EDMs.Business.Services.Security;

    public partial class Login : Page
    {
        #region Fields
        private readonly UserService _userService;
        #endregion
        
        public Login()
        {
            _userService = new UserService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            txtUsername.Focus();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = Utility.GetMd5Hash(txtPassword.Text);

            var user = _userService.GetUserByUsername(username);
            if (user != null && user.Password == password)
            {
                UserSession.CreateSession(user);
                FormsAuthentication.RedirectFromLoginPage(user.Username, false);
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.Clear();
                FormsAuthentication.SignOut();
                lblMessage.Text = "Username or password is incorrect.";
            }
        }
    }
}