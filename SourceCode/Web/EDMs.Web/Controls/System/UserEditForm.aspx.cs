﻿namespace EDMs.Web
{
    using System;
    using System.Configuration;
    using System.Drawing;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    public partial class UserEditForm : Page
    {

        #region Fields
        private readonly UserService userService;
        private readonly RoleService roleService;

        private const string ResourceParameterKey = "id";
        private const string UserParameterKey = "userid";
        #endregion

        #region Properties
        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        private int? ResourceId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[ResourceParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[ResourceParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        private int? UserId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[UserParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[UserParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has user account.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has user account; otherwise, <c>false</c>.
        /// </value>
        private bool HasUserAccount
        {
            get { return !string.IsNullOrEmpty(txtUsername.Text.Trim()); }
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Loads the data to combo.
        /// </summary>
        private void LoadDataToCombo()
        {
            var roles = this.roleService.GetAll(UserSession.Current.RoleId == 1);
            roles.Insert(0, new Role() { Id = 0, Name = string.Empty });
            this.ddlRoles.DataSource = roles;
            this.ddlRoles.DataValueField = "Id";
            this.ddlRoles.DataTextField = "Name";
            this.ddlRoles.DataBind();
        }

        #endregion

        #region Initializes
        
        public UserEditForm()
        {
            this.userService=new UserService();
            this.roleService = new RoleService();
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtUsername.Focus();
                LoadDataToCombo();
                if (!string.IsNullOrEmpty(this.Request.QueryString["userId"]))
                {
                    var userId = Convert.ToInt32(this.Request.QueryString["userId"]);
                    var user = this.userService.GetByID(userId);
                    if (user != null)
                    {
                        this.txtUsername.Text = user.Username;
                        this.txtEmail.Text = user.Email;
                        this.txtCellPhone.Text = user.CellPhone;
                        this.txtHomePhone.Text = user.Phone;
                        this.txtPosition.Text = user.Position;
                        this.txtFullName.Text = user.FullName;
                        this.ddlRoles.SelectedValue = user.RoleId.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCapNhat control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["userId"]))
                {
                    var userId = Convert.ToInt32(this.Request.QueryString["userId"]);
                    var user = this.userService.GetByID(userId);
                    if (user != null)
                    {
                        user.Username = this.txtUsername.Text.Trim();
                        user.FullName = this.txtFullName.Text.Trim();
                        user.RoleId = Convert.ToInt32(this.ddlRoles.SelectedValue);
                        user.Position = this.txtPosition.Text.Trim();
                        user.Email = this.txtEmail.Text.Trim();
                        user.Phone = this.txtHomePhone.Text.Trim();
                        user.CellPhone = this.txtCellPhone.Text.Trim();

                        this.userService.Update(user);
                    }
                }
                else
                {
                    var user = new User
                    {
                        Username = this.txtUsername.Text.Trim(),
                        Password = Utility.GetMd5Hash(GlobalVariables.Current.DefaultPasswordForNewUser),
                        FullName = this.txtFullName.Text.Trim(),
                        RoleId = Convert.ToInt32(this.ddlRoles.SelectedValue),
                        Position = this.txtPosition.Text.Trim(),
                        Email = this.txtEmail.Text.Trim(),
                        Phone = this.txtHomePhone.Text.Trim(),
                        CellPhone = this.txtCellPhone.Text.Trim()
                    };

                    this.userService.Insert(user);
                }

                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        #endregion

        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            
        }

        protected void ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtUsername.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter User name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else if (this.HasUserAccount)
            {
                this.fileNameValidator.ErrorMessage = "The user name is already in use.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = !this.userService.CheckExists(this.UserId, this.txtUsername.Text.Trim());
            }
        }
    }
}